pub mod prelude;

use std::convert::TryFrom;
use std::future::Future;
use std::num::NonZeroI32;
use std::fmt::{Display, Formatter, Write};
use std::ops::{Deref, DerefMut};
use std::pin::Pin;

use futures::future::join_all;
use tokio::process::Command;
use futures::StreamExt;



#[derive(Debug)]
pub struct Error {
    e: ErrorKind,
    backtrace: Vec<String>,
}

impl Error {
    pub fn new(e: ErrorKind, backtrace: Vec<String>) -> Error {
        Error { e, backtrace }
    }
    pub fn error(&self) -> &ErrorKind {
        &self.e
    }
    pub fn into_err(self) -> ErrorKind {
        self.e
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        writeln!(f, "An error occurred during the execution of command {}", self.backtrace.last().unwrap_or(&String::from("unknown")))?;
        writeln!(f, "{}", self.e)?;
            writeln!(f, "Backtrace:")?;
        for (i, it) in self.backtrace.iter().enumerate() {
            writeln!(f, "{}{}", "  ".repeat(i), it)?;
        }
        Ok(())
    }
}

impl From<ErrorKind> for Error {
    fn from(other: ErrorKind) -> Self {
        Error{ e: other, backtrace: Vec::new() }
    }
}

impl From<ErrorKind> for Vec<Error> {
    fn from(other: ErrorKind) -> Self {
        vec![Error{ e: other, backtrace: Vec::new() }]
    }
}

#[derive(Debug)]
pub enum ErrorKind {
    Utf8(std::string::FromUtf8Error),
    IO(std::io::Error),
    Status(Option<NonZeroI32>, Option<String>),
    Collection(Vec<Error>),
    None,
}

impl Display for ErrorKind {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        match self {
            ErrorKind::IO(e) => write!(f, "{}", e),
            ErrorKind::Utf8(e) => write!(f, "{}", e),
            ErrorKind::Status(status, stderr) => {
                if let Some(status) = status {
                    writeln!(f, "Command executed with status {}", status)?;
                }
                if let Some(stderr) = stderr {
                    writeln!(f, "Output of command:")?;
                    writeln!(f, "{}", stderr)?;
                }
                Ok(())
            },
            ErrorKind::Collection(e) => {
                writeln!(f, "{} Errors:", e.len())?;
                for e in e {
                    writeln!(f, "{}", e)?;
                }
                Ok(())
            },
            ErrorKind::None => {
                write!(f, "None error. This error should aways be ignored!")
            },
        }
    }
}

impl From<std::string::FromUtf8Error> for ErrorKind {
    fn from(other: std::string::FromUtf8Error) -> Self {
        ErrorKind::Utf8(other)
    }
}

impl From<std::io::Error> for ErrorKind {
    fn from(other: std::io::Error) -> ErrorKind {
        ErrorKind::IO(other)
    }
}

impl From<(Option<NonZeroI32>, Option<String>)> for ErrorKind {
    fn from(other: (Option<NonZeroI32>, Option<String>)) -> ErrorKind {
        ErrorKind::Status(other.0, other.1)
    }
}

impl From<Vec<Error>> for ErrorKind {
    fn from(other: Vec<Error>) -> Self {
        ErrorKind::Collection(other)
    }
}

impl From<Error> for ErrorKind {
    fn from(other: Error) -> Self {
        other.e
    }
}

#[derive(Debug)]
pub struct CommandOwn(pub Command);

impl From<Command> for CommandOwn {
    fn from(cmd: Command) -> Self {
        Self(cmd)
    }
}

impl From<CommandOwn> for Command {
    fn from(other: CommandOwn) -> Self {
        other.0
    }
}

impl CommandOwn {
    pub fn get_name(&self) -> String {
        let mut name: String = self.0.as_std().get_program().to_string_lossy().to_string();
        for arg in self.0.as_std().get_args() {
            // Writing to a string never errors
            write!(name, " {}", arg.to_string_lossy()).unwrap();
        }
        if let Some(current_dir) = self.0.as_std().get_current_dir() {
            // Writing to a string never errors
            write!(name, "\t[in {}]", current_dir.to_string_lossy()).unwrap();
        }
        name
    }
}

pub trait FnClone: FnOnce() -> Result<(), ErrorKind> {
    fn call(&self) -> Result<(), ErrorKind>;
}

impl<T: FnOnce() -> Result<(), ErrorKind> + Clone + 'static> FnClone for T {
    fn call(&self) -> Result<(), ErrorKind> {
        self.clone()()
    }
}

pub trait CommandTrt<'a, T> {
    fn exec(self, name: impl Into<String>) -> Pin<Box<dyn Future<Output = Result<T, Vec<Error>>> + Send + 'a>>;
}

impl CommandTrt<'static, ()> for CommandOwn {
    fn exec(mut self, name: impl Into<String>) -> Pin<Box<dyn Future<Output = Result<(), Vec<Error>>> + Send + 'static>>
    {
        let name: String = name.into();
        let inner_name = self.get_name();
        let fut = async move {
            match self.0.spawn() {
                Ok(output) => match output.wait_with_output().await {
                    Ok(output) => match output.status.code() {
                        Some(0) => Ok(()),
                        s => Err(vec![Error{
                            e: ErrorKind::Status(s.map(|s| NonZeroI32::try_from(s).unwrap()), Some(String::from_utf8_lossy(&output.stderr).into())),
                            backtrace: vec![name.into()]
                        }]),
                    },
                    Err(e) => Err(vec![Error{ e: ErrorKind::IO(e), backtrace: vec![inner_name, name.into()] }]),
                },
                Err(e) => Err(vec![Error{ e: ErrorKind::IO(e), backtrace: vec![inner_name, name.into()] }])
            }
        };
        Box::pin(fut)
    }
}

trait InnerCommandTrt<'a, T>
{
    //type Inner: DerefMut + Unpin + Send + 'static;
    fn exec(self, name: impl Into<String>) -> Pin<Box<dyn Future<Output = Result<T, Vec<Error>>> + Send + 'a>>;
}

impl<'a, F: DerefMut + Unpin + Send + 'a, T: Send + 'a> InnerCommandTrt<'a, T> for (Pin<F>, Option<Result<T, Error>>)
    where <F as Deref>::Target: Future<Output= Result<T, Error>> + Send
{
    //type Inner = Pin<Box<F>>;
    fn exec(self, name: impl Into<String>) -> Pin<Box<dyn Future<Output = Result<T, Vec<Error>>> + Send + 'a>> {
        let name: String = name.into();
        let fut = async move {
            let res = self.0.await;
            res.map_err(|mut e| {
                e.backtrace.push(name.clone());
                vec![e]
            })
        };
        Box::pin(fut)
    }
}
impl<'a, F: DerefMut + Unpin + Send + 'a, T: Send + 'a> InnerCommandTrt<'a, T> for (Pin<F>, Option<Result<T, ErrorKind>>)
    where <F as Deref>::Target: Future<Output= Result<T, ErrorKind>> + Send
{
    //type Inner = Pin<Box<F>>;
    fn exec(self, name: impl Into<String>) -> Pin<Box<dyn Future<Output = Result<T, Vec<Error>>> + Send + 'a>> {
        let name: String = name.into();
        let fut = async move {
            let res = self.0.await;
            res.map_err(|e| vec![Error{ e, backtrace: vec![name] }])
        };
        Box::pin(fut)
    }
}
impl<'a, F: DerefMut + Unpin + Send + 'a, T: Send + 'a> InnerCommandTrt<'a, T> for (Pin<F>, Option<Result<T, Vec<Error>>>)
    where <F as Deref>::Target: Future<Output= Result<T, Vec<Error>>> + Send
{
    //type Inner = Pin<Box<F>>;
    fn exec(self, name: impl Into<String>) -> Pin<Box<dyn Future<Output = Result<T, Vec<Error>>> + Send + 'a>> {
        let name: String = name.into();
        let fut = async move {
            let mut res = self.0.await;
            if let Err(err) = &mut res {
                for e in err {
                    e.backtrace.push(name.clone());
                }
            }
            res
        };
        Box::pin(fut)
    }
}

impl<'a, T: std::ops::DerefMut + Unpin + Send + 'a, U> CommandTrt<'a, U> for Pin<T>
    where (Pin<T>, Option<<<T as Deref>::Target as Future>::Output>): InnerCommandTrt<'a, U>,
    <T as Deref>::Target: Future + Send + 'a
{
    fn exec(self, name: impl Into<String>) -> Pin<Box<dyn Future<Output = Result<U, Vec<Error>>> + Send + 'a>> {
        InnerCommandTrt::exec((self, None), name)
    }
}


pub async fn stack<'a, I: IntoIterator<Item = T>, T: CommandTrt<'a, R>, R>(cmds: I, name: impl Into<String>) -> Result<(), Vec<Error>> {
    let name: String = name.into();
    for cmd in cmds {
        if let Err(e) = cmd.exec(name.clone()).await {
            return Err(e);
        }
    }
    Ok(())
}
pub async fn or<'a, I: IntoIterator<Item = T>, T: CommandTrt<'a, R>, R>(cmds: I, name: impl Into<String>) -> Result<R, Vec<Error>> {
    let name: String = name.into();
    let mut err = Vec::new();
    for cmd in cmds {
        match cmd.exec(name.clone()).await {
            Ok(a) => return Ok(a),
            Err(e) => err.extend(e),
        }
    }
    Err(err)
}
pub async fn sequence<'a, I: IntoIterator<Item = T>, T: CommandTrt<'a, ()>>(cmds: I, name: impl Into<String>) -> Result<(), Vec<Error>>{
    let name: String = name.into();
    let mut err = Vec::new();
    for cmd in cmds {
        if let Err(e) = cmd.exec(name.clone()).await {
            err.extend(e);
        }
    }
    if err.is_empty() {
        Ok(())
    } else {
        Err(err)
    }
}
pub async fn unordered<'a, I: IntoIterator<Item = T>, T: CommandTrt<'a, ()>>(cmds: I, name: impl Into<String>) -> Result<(), Vec<Error>> {
    let name: String = name.into();
    let mut err = Vec::new();
    for res in join_all(cmds.into_iter()
        .map(move |cmd| cmd.exec(name.clone())))
        .await
    {
        if let Err(e) = res {
            err.extend(e);
        }
    }
    if err.is_empty() {
        Ok(())
    } else {
        Err(err)
    }
}

pub async fn parallel<I: IntoIterator<Item = T> + Send + 'static, T: CommandTrt<'static, ()> + Send>(cmds: I, name: impl Into<String>) -> Result<(), Vec<Error>>
    where <I as IntoIterator>::IntoIter: std::marker::Send
{
    let name: String = name.into();
    let mut rec = {
        let (ser, rec) = tokio::sync::mpsc::unbounded_channel();
        futures::stream::iter(cmds)
            .for_each_concurrent(num_cpus::get(), |cmd| {
                let ser = ser.clone();
                let name = name.clone();
                async move {
                    if let Err(e) = tokio::spawn(cmd.exec(name)).await.expect("Task could not be joined") {
                        ser.send(e).expect("Error of parallel operation could not be signaled.");
                    }
                }
            }).await;
        // Drop `ser` but retain `rec` such that the sent errors can be retrieved
        rec
    };

    let mut err = Vec::new();
    while let Some(err_new) = rec.recv().await {
        err.extend(err_new);
    }

    if err.is_empty() {
        Ok(())
    } else {
        Err(err)
    }
}

#[macro_export]
macro_rules! cmd_as_owned {
    ( $name:literal $( .$method:ident $arg:tt)* )  => {
        {
            let mut cmd = tokio::process::Command::new($name);
            cmd.stdout(std::process::Stdio::inherit());
            cmd.stderr(std::process::Stdio::piped()) $( .$method $arg )* ;
            $crate::CommandOwn(cmd)
        }
    };
    ( $( $name:literal $( .$method:ident $arg:tt)* ),* $(,)? )  => {
        vec![
            $({
                let mut cmd = tokio::process::Command::new($name);
                cmd.stdout(std::process::Stdio::inherit());
                cmd.stderr(std::process::Stdio::piped()) $( .$method $arg )* ;
                $crate::CommandOwn(cmd)
            },)*
        ]
    };
}